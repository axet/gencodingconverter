ROOT=$(shell pwd)
RPM=${ROOT}/rpm
VERSION=$(shell perl -lne 'm|Version: (.*)|g && print $$1' ${RPM}/SPECS/gencodingconverter.spec)
RELEASE=$(shell perl -lne 'm|Release: (.*)|g && print $$1' ${RPM}/SPECS/gencodingconverter.spec)

all:

.PHONY: clean

clean: crpm

.PHONY: rpm

rpm:
	mkdir -p ${RPM}/SOURCES
	tar -C ${ROOT} -czvf ${RPM}/SOURCES/gencodingconverter-${VERSION}.tgz src gencodingconverter.plugin
	rpmbuild --define '_topdir ${RPM}' -ba ${RPM}/SPECS/*.spec

crpm:
	rm -rf ${RPM}/BUILD
	rm -rf ${RPM}/BUILDROOT
	rm -rf ${RPM}/RPMS
	rm -rf ${RPM}/SOURCES
	rm -rf ${RPM}/SRPMS

irpm:
	sudo rpm -U --force ${RPM}/RPMS/noarch/gencodingconverter-${VERSION}-${RELEASE}.noarch.rpm
