Summary: gencodingconverter plugin
Name: gencodingconverter
Version: 1.2.0
Release: 1
License: GPL
Group: Applications/Sound
Source: http://code.google.com/p/gencodingconverter/gencodingconverter-1.2.0.tgz
URL: http://code.google.com/p/gencodingconverter/
Packager: Alexey Kuznetsov <ak@axet.ru>
BuildArch: noarch

%description
Just install it.

%prep
%setup -c

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/lib/gedit/plugins/gencodingconverter
cp src/* $RPM_BUILD_ROOT/usr/lib/gedit/plugins/gencodingconverter
cp *.plugin $RPM_BUILD_ROOT/usr/lib/gedit/plugins/

%files
/usr/lib/gedit/plugins/gencodingconverter/
/usr/lib/gedit/plugins/gencodingconverter.plugin
